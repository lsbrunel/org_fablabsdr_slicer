package org.fablabsdr.slicer.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.util.Vector;

import javax.swing.JFileChooser;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.geometry.Frame;
import com.photonlyx.toolbox.math.geometry.Plane;
import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.PolyLine3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.threeD.gui.Text3D;
import com.photonlyx.toolbox.threeD.io.STLFileFilter;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;

public class SlicerApp extends WindowApp
{
private Graph3DPanel panel3D=new Graph3DPanel();
private String stlfile;
private TriangleMesh tris=new TriangleMesh();
//unorganized segments arranged in slices:
private Vector<Vector<Segment3D>> rawSegmentsSlices=new Vector<Vector<Segment3D>>();
//ordered polylines (a slice may have many polylines): 
private Vector<Vector<PolyLine3D>> slices=new Vector<Vector<PolyLine3D>>();
private Vector<Vector<PolyLine3D>> slicesProjected=new Vector<Vector<PolyLine3D>>();
public double sliceThickness=1;
public double dirx=0,diry=0,dirz=1;

public boolean showstl=true;
public boolean showSegments=false;
public boolean showSlices=true;
private ParamsBox box2;

public SlicerApp()
{
super("org.fablabsdr.slicer",true,true,true);

//this.getPanelSide().setBackground(Color.green);
add(panel3D.getJPanel(),"3D view");

panel3D.add(tris);

panel3D.hidePanelSide();
panel3D.initialiseProjector();
panel3D.update();	

//button
{
TriggerList tl_button=new TriggerList();
tl_button.add(new Trigger(this,"selectSTLfile"));
tl_button.add(new Trigger(this,"loadSTLfile"));
tl_button.add(new Trigger(this,"sliceSTLfile"));
tl_button.add(new Trigger(this,"saveSVG"));
tl_button.add(new Trigger(this,"updateView"));
tl_button.add(new Trigger(panel3D,"update"));
tl_button.add(new Trigger(box2,"update"));
CButton cButton=new CButton(tl_button);
cButton.setPreferredSize(new Dimension(150, 20));
cButton.setIconFileName("org/fablabsdr/slicer/gui/STLImport.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("Import stl");
this.getToolBar().add(cButton);
}

//params
{
TriggerList tl=new TriggerList();
tl.add(new Trigger(this,"updateView"));
tl.add(new Trigger(panel3D,"update"));
String[] names1={"sliceThickness","dirx","diry","dirz","showstl","showSegments","showSlices"};
ParamsBox box1=new ParamsBox(this,tl,null,names1,null,null,ParamsBox.VERTICAL,150,30);
this.getPanelSide().add(box1.getComponent());
}

//params out
{
String[] names1={"nbSlices","height"};
box2=new ParamsBox(this,null,null,names1,null,null,ParamsBox.VERTICAL,150,30);
box2.setEditable(false);
//ParIntBox pb=((ParIntBox)box2.getParamBoxOfParam("nbSlices"));pb.setEditable(false);
//ParIntBox pb1=((ParIntBox)box2.getParamBoxOfParam("height"));pb.setEditable(false);
}

//button
{
TriggerList tl_button=new TriggerList();
tl_button.add(new Trigger(this,"sliceSTLfile"));
tl_button.add(new Trigger(this,"saveSVG"));
tl_button.add(new Trigger(this,"updateView"));
tl_button.add(new Trigger(panel3D,"update"));
tl_button.add(new Trigger(box2,"update"));
CButton cButton=new CButton(tl_button);
cButton.setPreferredSize(new Dimension(150, 20));
//cButton.setIconFileName("org/fablabsdr/slicer/gui/STLImport.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("Recalc");
this.getPanelSide().add(cButton);
}

//add params out
this.getPanelSide().add(box2.getComponent());


this.getPanelSide().validate();
this.getPanelSide().repaint();

this.getToolBar().validate();
this.getToolBar().repaint();


//stlfile="sphere.stl";
//loadSTLfile();
//sliceSTLfile();
//saveSVG();
//panel3D.update();
//updateView();
}


public void selectSTLfile()
{
JFileChooser df=new JFileChooser(path);
STLFileFilter ff=new STLFileFilter();
df.addChoosableFileFilter(ff);
df.setFileFilter(ff);
int returnVal = df.showOpenDialog(null);
if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	path=df.getSelectedFile().getParent()+File.separator;
	stlfile=df.getSelectedFile().getName();
	

	}	
}


public void loadSTLfile()
{
tris.removeAllElements();
tris.readSTLFile(path+File.separator+stlfile);
//tris.setColor(Color.LIGHT_GRAY);
}


public void sliceSTLfile()
{
Vecteur normal=new Vecteur(dirx,diry,dirz).getNormalised();
Vecteur[] cube=tris.getOccupyingCube();
Vecteur o=cube[0];

//slice:
slice(tris,normal,sliceThickness,rawSegmentsSlices,slices);

//put the slices in the same plane
Plane plane=new Plane(o,normal);
projectLayersOnSlicePlane(plane,slices,slicesProjected);

}


public void saveSVG()
{
//save the 2D slices in an svg file:
String filename=path+File.separator+stlfile.replace(".stl", "")+".svg";
saveLayersAsSVG(slicesProjected,filename);
System.out.println(filename+" "+"saved");
Global.setMessage(filename+" "+"saved");
	
}


public void updateView()
{
panel3D.removeAll3DObjects();

if (showstl)
	{
	//show stl in 3D view:
	panel3D.add(tris);
	}
if (showSegments)
	{
	System.out.println("showSegments");
	//show the segments:
	int c=0;
	for (Vector<Segment3D> segmentSlice:rawSegmentsSlices)
		for (Segment3D seg:segmentSlice) 
			{
			Segment3DColor seg2=new Segment3DColor(seg,Color.RED);
			Text3D t=new Text3D(""+c,seg.p1().addn(seg.p2()).scmul(0.5),Color.RED);
			panel3D.add(seg2);
			panel3D.add(t);
			c++;
			}
	}


if (showSlices)
{
//show the slices polylines:
for (Vector<PolyLine3D> slice:slices) 
	for (PolyLine3D pol:slice) 
		{
		//System.out.println(pol);
		//show the polyline:
		PolyLine3DColor plc=new PolyLine3DColor(pol,Color.getHSBColor((float)Math.random(), 1, 1));
		plc.setSeePoints(false);
		panel3D.add(plc);
//		//show the points counting:
//		int c1=0;
//		for (Vecteur p:pol)
//			{
//			Text3D t=new Text3D(""+c1,p,Color.BLUE);
//			panel3D.add(t);
//			c1++;
//			}
		}
}


//show the projected slices:
//for (Vector<PolyLine3D> layer:layers1) 
//	for (PolyLine3D pol:layer) 
//		{
//		panel3D.add(new PolyLine3DColor(pol,Color.GREEN));
//		}




}




public static void slice(TriangleMesh tris,Vecteur normal
		,double sliceThickness,Vector<Vector<Segment3D>> segmentsSlices,Vector<Vector<PolyLine3D>> slices)
{
//get the x size as typical size:
Vecteur[] cube=tris.getOccupyingCube();
double size=cube[1].sub(cube[0]).norme();

Vecteur o=cube[0];
//double delta=cube[1].x()-cube[0].x();
//double nbSlices=delta/sliceThickness;

//double sliceThickness=delta/nbSlices;
//System.out.println("Slice thickness="+sliceThickness);

slices.removeAllElements();
segmentsSlices.removeAllElements();
int i=0;
boolean began=false;
for (;;)
	{
	Plane plane=new Plane(o.addn(normal.scmul(sliceThickness*i)),normal);
	Vector<Segment3D> segs=tris.getIntersectedSegments(plane);
	segmentsSlices.add(segs);
	//System.out.println("Slice "+i+" "+segs.size()+" intersected segments");
	
	i++;
	if (segs.size()>=3) began=true;
	if ((segs.size()<3)&&(began)) break;;		
	if (i==200) break;;		
	if (segs.size()<3) continue;		
	
	double tolerance=0.0000001;
	Vector<PolyLine3D> polylines=transformConnectedSegmentsInPolyLines(segs,tolerance);
	//System.out.println("Slice "+(i-1)+" "+polylines.size()+" polylines");
	slices.add(polylines);
	

	}

System.out.println(slices.size()+" slices");
}


public static void projectLayersOnSlicePlane(Plane plane,
		Vector<Vector<PolyLine3D>> layers,Vector<Vector<PolyLine3D>> slicesProjected)
{
slicesProjected.removeAllElements();
Frame frame=plane.getLocalFrame();
for (Vector<PolyLine3D> layer:layers) 
	{
	Vector<PolyLine3D> layer1=new Vector<PolyLine3D>();
	slicesProjected.add(layer1);
	for (PolyLine3D pol:layer) 
		{
		PolyLine3D pol1=new PolyLine3D();
		layer1.add(pol1);
		for (Vecteur p:pol)
			{
			//project point on the slicer plane:
			Vecteur p1=plane.project(p);
			double x=frame.axis(0).mul(p1.sub(plane.centre()));
			double y=frame.axis(1).mul(p1.sub(plane.centre()));
			double z=0;
			//p.affect(x, y, z);
			pol1.addPoint(new Vecteur(x,y,z));
			}
		}
	}

}


public static void saveLayersAsSVG(Vector<Vector<PolyLine3D>> layers,String svgFilename)
{
//save the svg file:
StringBuffer sb=new StringBuffer();
sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
sb.append("<svg width=\"391\" height=\"391\" viewBox=\"-70.5 -70.5 391 391\" xmlns=\"http://www.w3.org/2000/svg\">");

Vector<PolyLine3D> layerMiddle=layers.elementAt(layers.size()/2);
double sizex=(layerxmax(layerMiddle)-layerxmin(layerMiddle))*1.2;
double sizey=(layerymax(layerMiddle)-layerymin(layerMiddle))*1.2;
double strockew=sizex/200;

int w=(int)Math.sqrt(layers.size());

int c=0;
for (Vector<PolyLine3D> layer:layers) 
	{
	int j=(int)Math.floor(c/w);
	int i=c-j*w;
	System.out.println(i+" "+j);
	//double deltax=(layerxmax(layer)-layerxmin(layer))*1.2;
	//double deltay=(layerymax(layer)-layerymin(layer))*1.2;
	double dx=sizex*i;
	double dy=sizey*j;
	for (PolyLine3D pol:layer) 
		{
		sb.append("<polyline points=\"");
		for (Vecteur p:pol)
			{
			sb.append((dx+p.x())+","+(dy+p.y())+",");
			}
		sb.append(" \" stroke=\"red\" stroke-width=\""+strockew+"\" fill=\"none\" /> \n");
		}
	c++;
	}
sb.append("</svg>");
TextFiles.saveString(svgFilename, sb.toString());
		
}

private static double layerymin(Vector<PolyLine3D> layer)
{
double d=1e30;
for (PolyLine3D pol:layer) 
	for (Vecteur v:pol) 
		if (v.y()<d) d=v.y();
return d;	
}

private static double layerymax(Vector<PolyLine3D> layer)
{
double d=-1e30;
for (PolyLine3D pol:layer) 
	for (Vecteur v:pol) 
		if (v.y()>d) d=v.y();
return d;	
}
private static double layerxmin(Vector<PolyLine3D> layer)
{
double d=1e30;
for (PolyLine3D pol:layer) 
	for (Vecteur v:pol) 
		if (v.x()<d) d=v.x();
return d;	
}

private static double layerxmax(Vector<PolyLine3D> layer)
{
double d=-1e30;
for (PolyLine3D pol:layer) 
	for (Vecteur v:pol) 
		if (v.x()>d) d=v.x();
return d;	
}

private static Vector<PolyLine3D> transformConnectedSegmentsInPolyLines(Vector<Segment3D> segs,double tolerance)
{
//will transform the segments in polylines:
Vector<PolyLine3D> polylines=new Vector<PolyLine3D>();
int indexPolyLine=0;
//add a first polyline:
PolyLine3D pol0=new PolyLine3D();
polylines.add(pol0);

Vector<Segment3D> segs2=(Vector<Segment3D>) segs.clone();
//for (Segment3D seg_:segs) System.out.println(seg_);

//get a first point:
Segment3D seg=segs2.elementAt(0);
Vecteur point=seg.p1();
//remove the current segment from the list of segments:
segs2.remove(seg);
for (;;)
	{
	//System.out.println("transformConnectedSegmentsInPolyLines:"+seg);
	//add the current point to the current polyline:
	polylines.elementAt(indexPolyLine).addPoint(point);
	//if there are no more segments in the list, exit the loop
	if (segs2.size()==0) 
		{
		//close the polyline:
		PolyLine3D pol1=polylines.elementAt(polylines.size()-1);
//		System.out.println("first point "+pol1.first());
//		System.out.println("last point "+pol1.last());
		pol1.addPoint(pol1.getPoint(0));
		break;
		}
	//find the other connected segment:
	int[] indexConnectionPoint=new int[1];	
	//System.out.println("Find connected segment to point  "+point+" (tolerance="+tolerance+")");

	Segment3D seg2=findConnectedSegment(segs2,point,tolerance,indexConnectionPoint);
	//System.out.println("found "+seg2);
	if (seg2==null) //can't find a connected segment
		{
		//break;
		if (segs2.size()>0)//there are still unconnected points, start a new polyline:
			{
			//create a new polyline:
			polylines.add(new PolyLine3D());
			indexPolyLine++;
			//work with a new segment and point:
			seg=segs2.elementAt(0);
			point=seg.p1();
			continue;
			}
//		else //no more segments to connect
//			{
//			break;
//			}
		}
//	System.out.println("transformConnectedSegmentsInPolyLines: connected segment: "+seg2);
//	System.out.println("transformConnectedSegmentsInPolyLines: now work on point "+seg2.point(1-indexConnectionPoint[0]));
	//add the other point of the segment to the current polyline:
	//polylines.elementAt(indexPolyLine).addPoint(seg2.point(1-indexConnectionPoint[0]));
	//now work on the connected segment
	seg=seg2;
	//now work on the other point:
	point=seg2.point(1-indexConnectionPoint[0]);
	//System.out.println("transformConnectedSegmentsInPolyLines: now work on point "+point);
	//remove the found segment from the list of segments:
	segs2.remove(seg2);
	}	
//System.out.println(polylines.size()+" polyLines:");
return polylines;
}




/**
 * find the segment having one of its points very close to the point
 * @param segs
 * @param point
 * @return null if no connected segment found
 */
private static  Segment3D findConnectedSegment(Vector<Segment3D> segs,Vecteur point,double tolerance,int[] indexConnectionPoint)
{
//System.out.println("findConnectedSegment with the point: "+point.toString());
for (Segment3D seg:segs) 
	{
	//System.out.println("findConnectedSegment check:"+seg.toString());
	if (Vecteur.distance(seg.p1(),point)<=tolerance) 
		{
		indexConnectionPoint[0]=0;
		//System.out.println("findConnectedSegment found, index:"+indexConnectionPoint[0]);
		return seg;
		}
	if (Vecteur.distance(seg.p2(),point)<=tolerance) 
		{
		indexConnectionPoint[0]=1;
		//System.out.println("findConnectedSegment found, index:"+indexConnectionPoint[0]);
		return seg;
		}
	}
//System.out.println("findConnectedSegment not found");
return null;
}



public static void main(String[] args)
{
	SlicerApp slicerApp =new SlicerApp();
}
	
public int getNbSlices()
{
return slices.size();
}

public double getHeight()
{
Vecteur[] cube=tris.getOccupyingCube();
return (cube[1].z()-cube[0].z());
}
}
