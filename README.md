# org_fablabsdr_slicer


## Slicer
Transform a STL (ASCII) 3D model in a set of slices (in a SVG file).

![screenshot1](livraison/screenshot1.png)


## Installation

Install java (11+)

Download  https://gitlab.com/lsbrunel/org_fablabsdr_slicer/-/raw/main/livraison/org_fablabsdr_slicer.jar

Start with java -jar org_fablabsdr_slicer.jar

